import React from 'react';
import './InputPanel.css';
import FormControl from "material-ui/es/Form/FormControl";
import InputLabel from "material-ui/es/Input/InputLabel";
import Button from "material-ui/es/Button/Button";
import Input from "material-ui/es/Input/Input";

const InputPanel = ({ changedAuthor, changedMessage, postMessage, keyPressed }) => {
  return(
    <div className='InputPanel'>
      <FormControl>
        <InputLabel htmlFor="name-input">Name</InputLabel>
        <Input id="name-input" className='Input Input-name'
               onChange={(event) => changedAuthor(event)}/>
      </FormControl>
      <FormControl>
        <InputLabel htmlFor="name-input" >Message</InputLabel>
        <Input id="name-input" className='Input Input-message'
               onChange={(event) => changedMessage(event)}
               onKeyDown={(event) => keyPressed(event)}/>
      </FormControl>
      <Button variant="raised" size="small" onClick={postMessage}>Send</Button>
    </div>
  )
};

export default InputPanel;