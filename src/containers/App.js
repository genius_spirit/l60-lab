import React, { Component } from 'react';
import './App.css';
import InputPanel from "../components/InputPanel/InputPanel";
import ViewPanel from "../components/ViewPanel/ViewPanel";

const URL = 'http://146.185.154.90:8000/messages';

class App extends Component {

  state = {
    message: '',
    author: '',
    dateTime: '',
    messages: [],
  };

  interval = null;
  myId = [];

  handlerChangeAuthor = (event) => {
    this.setState({author: event.target.value})
  };

  handlerChangeMessage = (event) => {
    this.setState({message: event.target.value});
  };

  postMessage = () => {
    const data = new URLSearchParams();
    data.append('message', this.state.message);
    data.append('author', this.state.author);

    fetch(URL, { method: 'POST', body: data })
      .then(response => {
        if (response.ok) {return response.json();}
        throw new Error('Something wrong'); })
      .then(message => { this.myId.push(message._id) })
      .catch(error => { console.log(error);
    })
  };

  getPostsInterval = () => {
    this.interval = setInterval(() => {
      fetch(URL + '?datetime=' + this.state.dateTime).then(response => {
        if (response.ok) { return response.json();}
        throw new Error('Something wrong'); })
        .then(messages => {
          if (messages.length > 0) {
            this.setState({
              dateTime: messages[messages.length -1].datetime,
              messages: this.state.messages.concat(messages)
            }, () => this.scrollToButtom())
          }
        })
    }, 3000);
  }

  scrollToButtom = () => {
    const element = document.getElementsByClassName("Messages")[0];
    element.scrollTop = element.scrollHeight;
  }

  onKeyPressed =(event) => {
    if (event.keyCode === 13) {
      this.postMessage();
    }
  }

  componentDidMount() {
    this.getPostsInterval();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div className='App'>
        <ViewPanel messages={this.state.messages} myMessagesId={this.myId}/>
        <InputPanel
          changedAuthor={this.handlerChangeAuthor}
          changedMessage={this.handlerChangeMessage}
          postMessage={this.postMessage}
          keyPressed={this.onKeyPressed}/>
      </div>
    );
  }
}

export default App;
